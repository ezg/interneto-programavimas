var Kupra = window.Kupra || {};

Kupra.events = {};
Kupra.bindings = {};

Kupra.Core = {

    eventsHolder: 'data-event',
    bindsHolder: 'data-bind',

    eventPublisher: function (target, event) {
        if (!$(target).attr(Kupra.Core.eventsHolder)) {
            return true;
        }
        var eventsList = $(target).attr(Kupra.Core.eventsHolder).split(/\s+/);
        var responseStatus = true;
        $.each( eventsList, function(index, item){
            if (typeof window['Kupra']['events'][item] != "undefined") {
                responseStatus = window['Kupra']['events'][item]['run'](event);
            }
        });
        return responseStatus;
    },

    bindingsPublisher: function(target) {
        if (!$(target).attr(Kupra.Core.bindsHolder)) {
            return true;
        }
        var bindsList = $(target).attr(Kupra.Core.bindsHolder).split(/\s+/);
        $.each(bindsList, function(index, item) {
            if (typeof window['Kupra']['bindings'][item] != "undefined") {
                window['Kupra']['bindings'][item]['run']($(target));
            }
        });
    },

    get: function (key) {
        if (typeof window['Kupra']['Core']['events'][key] != "undefined") {
            return window['Kupra']['Core']['events'][key];
        } else {
            return false;
        }
    },

    set: function (key, val) {
        window['Kupra']['Core']['events'][key] = val;
    },

    split: function (val) {
        return val.split( /,\s*/ );
    },

    extractLast: function (term) {
        return Kupra.Core.split( term ).pop();
    },

    generateGuid: function() {
        var S4 = function() {
            return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
        };
        return (S4()+S4()+S4()+S4()+S4()+S4()+S4()+S4());
    }
};

$(document).ready(function(){

    var routeId = '';
    if ($('body').attr("data-controller-handler")) {
        var routeId = $('body').attr("data-controller-handler");
        if (typeof window[routeId] !== 'undefined') {
            window[routeId]["run"]();
        }
    }

    // click handlers
    $(document).click(function(event) {
        if ($(event.target).data('event-disable-click-handlers') == true) {
            return true;
        }
        return Kupra.Core.eventPublisher(event.target, event);
    });

    $.each($('body').find('[data-bind]'), function(target) {
        Kupra.Core.bindingsPublisher($(this));
    });

    // change handlers
    $(document).on('change', null, function(event) {
        return Kupra.Core.eventPublisher(event.target, event);
    });
});
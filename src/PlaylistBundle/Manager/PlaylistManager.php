<?php

namespace PlaylistBundle\Manager;

use PlaylistBundle\Entity\Playlist;
use PlaylistBundle\EntityRepository\PlaylistRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

/**
 * Class PlaylistManager
 * @package PlaylistBundle\Manager
 */
class PlaylistManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var EncoderFactory
     */
    private $encodeFactory;

    /**
     * @var PlaylistRepository
     */
    private $playlistRepository;

    /**
     * @param EntityManager $entityManager
     * @param EncoderFactory $encodeFactory
     * @param PlaylistRepository $playlistRepository
     */
    public function __construct(
        EntityManager $entityManager,
        EncoderFactory $encodeFactory,
        PlaylistRepository $playlistRepository
    ) {
        $this->entityManager = $entityManager;
        $this->encodeFactory = $encodeFactory;
        $this->playlistRepository = $playlistRepository;
    }

    /**
     * @return Playlist
     */
    public function create()
    {
        $playlist = new Playlist();

        return $playlist;
    }

    /**
     * @param Playlist $playlist
     * @return Playlist
     */
    public function register(Playlist $playlist)
    {
        $this->entityManager->persist($playlist);
        $this->entityManager->flush();

        return $playlist;
    }

    /**
     * @param Playlist $playlist
     * @return $this
     */
    public function update(Playlist $playlist)
    {
        $this->entityManager->persist($playlist);
        $this->entityManager->flush();

        return $this;
    }

    /**
     * @param Playlist $playlist or Array of playlists Ids
     * @return $this
     */
    public function remove($playlist)
    {
        if ($playlist instanceof Playlist) {
            $this->entityManager->remove($playlist);
        } elseif (is_array($playlist)) {
            foreach ($playlist as $playlistId) {
                if (is_numeric($playlistId)) {
                    $playlistEntity = $this->playlistRepository->findOneById($playlistId);
                    $this->entityManager->remove($playlistEntity);
                }
            }
        } elseif (is_numeric($playlist)) {
            $playlistEntity = $this->playlistRepository->findOneById($playlist);
            $this->entityManager->remove($playlistEntity);
        }

        $this->entityManager->flush();
        return $this;
    }
}
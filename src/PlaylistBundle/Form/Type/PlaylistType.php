<?php

namespace PlaylistBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class PlaylistType
 * @package PlaylistBundle\Form\Type
 */
class PlaylistType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('public', 'checkbox', [
                'required' => false
            ])
            ->add('songs')
            ->add('save', 'submit', [
                'label' => 'Išsaugoti',
            ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PlaylistBundle\Entity\Playlist',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'form_playlist_type';
    }
}
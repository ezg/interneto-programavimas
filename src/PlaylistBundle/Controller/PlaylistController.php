<?php

namespace PlaylistBundle\Controller;

use PlaylistBundle\Entity\Playlist;
use PlaylistBundle\Form\Type\PlaylistType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PlaylistController
 * @package PlaylistBundle\Controller
 *
 * @Route("/playlist")
 */
class PlaylistController extends Controller
{

    /**
     * @Route("/", name="_playlist_index")
     * @Template()
     */
    public function indexAction()
    {
        $repository = $this->get('repository.playlist');
        $playlists = $repository->findBy(array(), array('id' => 'ASC'));
        return array(
            'playlists' => $playlists,
        );
    }

    /**
     * @Route("/add", name="_playlist_add")
     * @Template()
     */
    public function addAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $manager = $this->get('manager.playlist');
        $playlist = $manager->create();
        $form = $this->createForm(new PlaylistType(), $playlist);
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST') {
            if ($form->isValid()) {
                $playlist->setDate(new \DateTime());
                $playlist->setOwner($this->getUser());
                $manager->register($playlist);

                return $this->redirect($this->generateUrl('_playlist_view', array('id' => $playlist->getId(),
                )));
            }
        }
        return array(
            'playlist' => $playlist,
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/edit/{id}", name="_playlist_edit")
     * @Template()
     */
    public function editAction($id, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $repository = $this->get('repository.playlist');
        $manager = $this->get('manager.playlist');

        $playlist = $repository->findOneById($id);

        if (!$playlist instanceof Playlist || $playlist->getOwner()->getId() != $this->getUser()->getId()) {
            $this->redirect('/');
        }

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new PlaylistType(), $playlist);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($playlist);
            $em->flush();
        }
        return array(
            'playlist' => $playlist,
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/delete/{id}", name="_playlist_delete")
     * @Template()
     */
    public function deleteAction($id, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $repository = $this->get('repository.playlist');
        $item = $repository->findOneById($id);
        if (!$item instanceof Playlist || $item->getOwner()->getId() != $this->getUser()->getId()) {
            $this->redirect('/');
        }
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        $manager = $this->get('manager.playlist');
        $manager->remove($item);
        return $this->redirect('/');
    }

    /**
     * @Route("/{id}", name="_playlist_view", requirements={"id" = "\d+"})
     * @Template()
     */
    public function viewAction($id, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $repository = $this->get('repository.playlist');
        $playlist = $repository->findOneById($id);

        if (!$playlist instanceof Playlist || ($playlist->getOwner()->getId() != $this->getUser()->getId() && $playlist->getPublic() != true)) {
            return $this->redirect('/');
        }

        return array(
            'playlist' => $playlist,
        );
    }

}

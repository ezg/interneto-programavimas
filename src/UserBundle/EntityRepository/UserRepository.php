<?php

namespace UserBundle\EntityRepository;

use Doctrine\ORM\EntityRepository;

/**
 * Class UserRepository
 * @package UserBundle\EntityRepository
 */
class UserRepository extends EntityRepository
{
}

<?php

namespace UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class UserProfileType
 * @package UserBundle\Form\Type
 */
class UserProfileType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', [
                'label' => 'Prisijungimo vardas',
                'required' => true,
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('password', 'repeated', array(
                'type' => 'password',
                'invalid_message' => 'Slaptažodžiai turi sutapti',
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => true,
                'first_options'  => array(
                    'label' => 'Slaptažodis',
                    'attr' => [
                        'class' => 'form-control',
                        'type' => 'password',
                        'placeholder' => 'Slaptažodis'
                    ]
                ),
                'second_options' => array(
                    'label' => 'Pakartokite',
                    'attr' => [
                        'class' => 'form-control',
                        'type' => 'password',
                        'placeholder' => 'Pakartokite slaptažodį'
                    ]
                ),
            ))
            ->add('save', 'submit', [
                'label' => 'Registruotis',
            ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\User',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'form_user_profile_type';
    }
}
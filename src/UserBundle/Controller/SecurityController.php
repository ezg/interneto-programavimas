<?php

namespace UserBundle\Controller;

use UserBundle\Form\Type\UserProfileType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="_login")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction()
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render('UserBundle:Security:login.html.twig', array(
            // last username entered by the user
            'lastUsername' => $session->get(SecurityContext::LAST_USERNAME),
            'error'         => $error,
        ));
    }

    /**
     * @Route("/register", name="_register")
     * @Template()
     */
    public function registerAction(Request $request)
    {
        $userRepository = $this->get('repository.core.user');
        $userManager = $this->get('manager.core.user');
        $user = $userManager->create();

        $form = $this->createForm(new UserProfileType(), $user);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $user->setSalt(md5(uniqid()));
                $user->setRole('USER');
                $user->setEnabled(true);
                $encoder = $this->get('security.encoder_factory')->getEncoder($user);
                $user->setPassword($encoder->encodePassword($user->getPassword(), $user->getSalt()));
                $em = $this->getDoctrine()->getManager();

                $em->persist($user);
                $em->flush();

                return $this->redirect($this->generateUrl('_default_route'));
            }
        }

        return [
            'form' => $form->createView(),
            'user' => $user,
        ];
    }

    /**
     * @Route("/admin/", name="_admin")
     */
    public function adminAction()
    {
        return $this->redirect($this->generateUrl('_admin_dashboard_index'));
    }

    /**
     * The security layer will intercept this request
     * @Route("/admin/login_check", name="_security_check")
     */
    public function securityCheckAction()
    {
    }

    /**
     * The security layer will intercept this request
     * @Route("/admin/logout", name="_logout")
     */
    public function logoutAction()
    {
    }

}
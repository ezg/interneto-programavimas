<?php

namespace UserBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CoreExtension
 * @package UserBundle\Twig
 */
class CoreExtension extends \Twig_Extension
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(
        ContainerInterface $container
    )
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            'camelize' => new \Twig_Filter_Method($this, 'camelizeFilter')
        ];
    }

    /**
     * @param $value
     * @return string
     */
    public function camelizeFilter($value)
    {
        if(!is_string($value)) {
            return $value;
        }
        $chunks = explode('_', $value);
        return implode('', array_map(function($s) { return ucfirst($s); }, $chunks));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'core_extension';
    }
}

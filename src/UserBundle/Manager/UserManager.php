<?php

namespace UserBundle\Manager;

use UserBundle\Entity\User;
use UserBundle\EntityRepository\UserRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

/**
 * Class UserManager
 * @package UserBundle\Manager
 */
class UserManager {

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var EncoderFactory
     */
    private $encodeFactory;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param EntityManager $entityManager
     * @param EncoderFactory $encodeFactory
     * @param UserRepository $userRepository
     */
    public function __construct(
        EntityManager $entityManager,
        EncoderFactory $encodeFactory,
        UserRepository $userRepository
    )
    {
        $this->entityManager = $entityManager;
        $this->encodeFactory = $encodeFactory;
        $this->userRepository = $userRepository;
    }

    /**
     * @return User
     */
    public function create()
    {
        $user = new User();

        return $user;
    }

    /**
     * @param User $user
     * @return User
     */
    public function register(User $user)
    {
        $user->setSalt(md5(uniqid()));
        $encoder = $this->encodeFactory->getEncoder($user);
        $encodedPassword = $encoder->encodePassword($user->getPassword(), $user->getSalt());
        $user->setPassword($encodedPassword);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    /**
     * @param User $user
     * @param null $originalPassword
     * @return $this
     */
    public function update(User $user, $originalPassword = null)
    {
        if ($user->getPassword()) {
            $encoder = $this->encodeFactory->getEncoder($user);
            $encodedPassword = $encoder->encodePassword($user->getPassword(), $user->getSalt());
            $user->setPassword($encodedPassword);
        } elseif ($originalPassword !== null) {
            $user->setPassword($originalPassword);
        }

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $this;
    }

    /**
     * @param User $user or Array of user Ids
     * @return $this
     */
    public function remove($user)
    {
        if($user instanceof User){
            $this->entityManager->remove($user);
        } else if(is_array($user)) {
            foreach($user as $userId){
                if(is_numeric($userId)){
                    $userEntity = $this->userRepository->findOneById($userId);
                    $this->entityManager->remove($userEntity);
                }
            }
        } else if(is_numeric($user)){
            $userEntity = $this->userRepository->findOneById($user);
            $this->entityManager->remove($userEntity);
        }

        $this->entityManager->flush();
        return $this;
    }
}
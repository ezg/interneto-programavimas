/**
 * Manages any type of collection with provided prototype
 * @type {{run: Function}}
 */
Kupra.events.addCollectionItem = {
    run: function (event) {

        var $collectionHolder = $(event.target).parents('table:first');
        var prototype = $collectionHolder.attr('data-prototype');
        var prototypeCallback = $collectionHolder.attr('data-prototype-callback');
        var indexName = $collectionHolder.attr('data-prototype-index-name');

        if (!indexName) {
            var indexNamePattern = new RegExp('__name__', 'g');
        } else {
            var indexNamePattern = new RegExp(indexName, 'g');
        }

        var newIndex = $collectionHolder.find('tbody:first > tr[data-owns-next!="true"]').length;

        var newForm = prototype.replace(indexNamePattern, newIndex);
        $collectionHolder.find('tbody:first').append(newForm);

        if (typeof window['Kupra']['events'][prototypeCallback] != "undefined") {
            responseStatus = window['Kupra']['events'][prototypeCallback]['run'](event, $collectionHolder);
        }

        return false;
    }
};



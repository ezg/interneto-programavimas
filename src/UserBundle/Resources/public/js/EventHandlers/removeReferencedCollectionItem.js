/**
 * @type {{run: Function}}
 */
Kupra.events.removeReferencedCollectionItem = {
    run: function (event) {

        var $collectionHolder = $(event.target).parents('table:first');
        var prototypeCallback = $collectionHolder.attr('data-prototype-callback');
        var $collectionItem = $(event.target).parents('tr:first');
        if ($collectionItem.attr('data-owns-next')) {
            $collectionItem.next().remove();
        }
        $collectionItem.remove();
        if (typeof window['Kupra']['events'][prototypeCallback] != "undefined") {
            responseStatus = window['Kupra']['events'][prototypeCallback]['run'](event, $collectionHolder);
        }
        return false;
    }
};



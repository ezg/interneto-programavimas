/**
 * @type {{run: Function}}
 */
Kupra.events.addReferencedCollectionItem = {
    run: function (event) {

        var $collectionHolder = $(event.target).parents('table:first');
        var prototype = $collectionHolder.attr('data-prototype');
        var prototypeCallback = $collectionHolder.attr('data-prototype-callback');
        var indexName = $collectionHolder.attr('data-prototype-index-name');

        if (!indexName) {
            var indexNamePattern = new RegExp('__name__', 'g');
        } else {
            var indexNamePattern = new RegExp(indexName, 'g');
        }

        var newIndex = KupraGoplanet.Core.generateGuid();
        var newForm = prototype.replace(indexNamePattern, newIndex);
        $collectionHolder.find('tbody:first').append(newForm).find('select').chosen();

        if (typeof window['Kupra']['events'][prototypeCallback] != "undefined") {
            responseStatus = window['Kupra']['events'][prototypeCallback]['run'](event, $collectionHolder);
        }

        return false;
    }
};



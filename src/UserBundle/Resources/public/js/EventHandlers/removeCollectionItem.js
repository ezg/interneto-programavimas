/**
 *
 * @type {{run: Function}}
 */
Kupra.events.removeCollectionItem = {
    run: function (event) {

        var $collectionHolder = $(event.target).parents('table:first');
        var prototypeCallback = $collectionHolder.attr('data-prototype-callback');
        var $collectionItem = $(event.target).parents('tr:first');
        if ($collectionItem.attr('data-owns-next')) {
            $collectionItem.next().remove();
        }
        $collectionItem.remove();
        var scopeName = $collectionHolder.attr('data-prototype-scope');
        var currentIndex = -1;
        $collectionHolder.find('tbody:first > tr').each(function(i, item){
            var $item = $(item);
            if (!$item.prev().attr('data-owns-next')) {
                currentIndex++;
            }

            //cause html() doesn't return selected values anymore
            $item.find('input').each(function(){
                $(this).attr('value',$(this).val());
            });

            var itemHtml = $item.html();
            if (!scopeName) {
                itemHtml = itemHtml.replace(/_[\d]*_/g, '_'+currentIndex+'_');
                itemHtml = itemHtml.replace(/\[[\d]*\]/g, '['+currentIndex+']');
            } else {
                var nameRegExp = new RegExp(scopeName+"\\]\\[[0-9]\\]", 'g');
                itemHtml = itemHtml.replace(nameRegExp, scopeName+']['+currentIndex+']');
                var idRegExp = new RegExp(scopeName+"_[0-9]_", 'g');
                itemHtml = itemHtml.replace(idRegExp, scopeName+'_'+currentIndex+'_');
            }
            $item.html(itemHtml);
        });

        if (typeof window['Kupra']['events'][prototypeCallback] != "undefined") {
            responseStatus = window['Kupra']['events'][prototypeCallback]['run'](event, $collectionHolder);
        }

        return false;
    }
};



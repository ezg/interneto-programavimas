<?php

namespace MusicBundle\Controller;

use MusicBundle\Entity\Song;
use MusicBundle\Form\Type\SongType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SongController
 * @package MusicBundle\Controller
 *
 * @Route("/song")
 */
class SongController extends Controller
{
    /**
     * @Route("/", name="_song_index")
     * @Template()
     */
    public function indexAction()
    {
        $repository = $this->get('repository.song');
        $songs = $repository->findBy(array(), array('id' => 'ASC'), 30);
        return array(
            'songs' => $songs,
        );
    }

    /**
     * @Route("/add", name="_song_add")
     * @Template()
     */
    public function addAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $manager = $this->get('manager.song');
        $song = $manager->create();
        $form = $this->createForm(new SongType(), $song);
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST') {
            if ($form->isValid()) {
                $manager->register($song);

                return $this->redirect($this->generateUrl('_song_view', array('id' => $song->getId(),
                )));
            }
        }
        return array(
            'song' => $song,
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/edit/{id}", name="_song_edit")
     * @Template()
     */
    public function editAction($id, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $repository = $this->get('repository.song');
        $manager = $this->get('manager.song');

        $song = $repository->findOneById($id);

        if (!$song instanceof Song) {
            $this->redirect('/');
        }

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SongType(), $song);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($song);
            $em->flush();
        }
        return array(
            'song' => $song,
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/delete/{id}", name="_song_delete")
     * @Template()
     */
    public function deleteAction($id, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $repository = $this->get('repository.song');
        $item = $repository->findOneById($id);
        $manager = $this->get('manager.song');
        $manager->remove($item);
        return $this->redirect('/');
    }

    /**
     * @Route("/{id}", name="_song_view", requirements={"id" = "\d+"})
     * @Template()
     */
    public function viewAction($id)
    {
        $repository = $this->get('repository.song');
        $song = $repository->findOneById($id);

        if (!$song instanceof Song) {
            return $this->redirect('/');
        }

        return array(
            'song' => $song,
        );
    }

}

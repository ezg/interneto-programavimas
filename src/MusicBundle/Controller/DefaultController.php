<?php

namespace MusicBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="_default_route")
     * @Template()
     */
    public function indexAction()
    {
        $albumRepository = $this->get('repository.album');
        $artistRepository = $this->get('repository.artist');
        $songRepository = $this->get('repository.song');
        $genreRepository = $this->get('repository.genre');

        $albums = $albumRepository->findBy(array(), array('id' => 'DESC'), 10);
        $artists = $artistRepository->findBy(array(), array('id' => 'DESC'), 10);
        $songs = $songRepository->findBy(array(), array('id' => 'DESC'), 10);
        $genres = $genreRepository->findBy(array(), array('id' => 'DESC'), 10);

        return array(
            'albums' => $albums,
            'artists' => $artists,
            'songs' => $songs,
            'genres' => $genres
        );
    }
}

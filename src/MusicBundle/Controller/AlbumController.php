<?php

namespace MusicBundle\Controller;

use MusicBundle\Entity\Album;
use MusicBundle\Form\Type\AlbumType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AlbumController
 * @package MusicBundle\Controller
 *
 * @Route("/album")
 */
class AlbumController extends Controller
{
    /**
     * @Route("/", name="_album_index")
     * @Template()
     */
    public function indexAction()
    {
        $repository = $this->get('repository.album');
        $albums = $repository->findBy(array(), array('id' => 'ASC'), 30);
        return array(
            'albums' => $albums,
        );
    }

    /**
     * @Route("/add", name="_album_add")
     * @Template()
     */
    public function addAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $manager = $this->get('manager.album');
        $album = $manager->create();
        $form = $this->createForm(new AlbumType(), $album);
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST') {
            if ($form->isValid()) {
                $manager->register($album);

                return $this->redirect($this->generateUrl('_view_album', array('id' => $album->getId(),
                )));
            }
        }
        return array(
            'album' => $album,
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/edit/{id}", name="_album_edit")
     * @Template()
     */
    public function editAction($id, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $repository = $this->get('repository.album');
        $manager = $this->get('manager.album');

        $album = $repository->findOneById($id);

        if (!$album instanceof Album) {
            $this->redirect('/');
        }

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new AlbumType(), $album);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($album);
            $em->flush();
        }
        return array(
            'album' => $album,
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/delete/{id}", name="_album_delete")
     * @Template()
     */
    public function deleteAction($id, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $repository = $this->get('repository.album');
        $item = $repository->findOneById($id);
        $em = $this->getDoctrine()->getManager();
        foreach ($item->getSongs() as $song) {
            $em->remove($song);
        }
        $em->flush();
        $manager = $this->get('manager.album');
        $manager->remove($item);
        return $this->redirect('/');
    }

    /**
     * @Route("/{id}", name="_view_album", requirements={"id" = "\d+"})
     * @Template()
     */
    public function viewAction($id)
    {
        $repository = $this->get('repository.album');
        $album = $repository->findOneById($id);

        if (!$album instanceof Album) {
            return $this->redirect('/');
        }

        return array(
            'album' => $album,
        );
    }

}

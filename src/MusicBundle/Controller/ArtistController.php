<?php

namespace MusicBundle\Controller;

use MusicBundle\Entity\Artist;
use MusicBundle\Form\Type\ArtistType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ArtistController
 * @package MusicBundle\Controller
 *
 * @Route("/artist")
 */
class ArtistController extends Controller
{
    /**
     * @Route("/", name="_artist_index")
     * @Template()
     */
    public function indexAction()
    {
        $repository = $this->get('repository.artist');
        $artists = $repository->findBy(array(), array('id' => 'ASC'), 30);
        return array(
            'artists' => $artists,
        );
    }

    /**
     * @Route("/add", name="_artist_add")
     * @Template()
     */
    public function addAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $manager = $this->get('manager.artist');
        $artist = $manager->create();
        $form = $this->createForm(new ArtistType(), $artist);
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST') {
            if ($form->isValid()) {
                $manager->register($artist);

                return $this->redirect($this->generateUrl('_artist_view', array('id' => $artist->getId(),
                )));
            }
        }

        return array(
            'artist' => $artist,
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/edit/{id}", name="_artist_edit")
     * @Template()
     */
    public function editAction($id, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $repository = $this->get('repository.artist');
        $manager = $this->get('manager.artist');

        $artist = $repository->findOneById($id);

        if (!$artist instanceof Artist) {
            $this->redirect('/');
        }

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ArtistType(), $artist);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($artist);
            $em->flush();
        }
        return array(
            'artist' => $artist,
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/delete/{id}", name="_artist_delete")
     * @Template()
     */
    public function deleteAction($id, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $repository = $this->get('repository.artist');
        $item = $repository->findOneById($id);
        $em = $this->getDoctrine()->getManager();
        foreach ($item->getAlbums() as $album) {
            foreach ($album->getSongs() as $song) {
                $em->remove($song);
            }
            $em->remove($album);
            $em->flush();
        }
        $manager = $this->get('manager.artist');
        $manager->remove($item);
        return $this->redirect('/');
    }

    /**
     * @Route("/{id}", name="_artist_view", requirements={"id" = "\d+"})
     * @Template()
     */
    public function viewAction($id)
    {
        $repository = $this->get('repository.artist');
        $artist = $repository->findOneById($id);

        if (!$artist instanceof Artist) {
            return $this->redirect('/');
        }

        return array(
            'artist' => $artist,
        );
    }

}

<?php

namespace MusicBundle\Controller;

use MusicBundle\Entity\Genre;
use MusicBundle\Form\Type\GenreType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GenreController
 * @package MusicBundle\Controller
 *
 * @Route("/genre")
 */
class GenreController extends Controller
{
    /**
     * @Route("/", name="_genre_index")
     * @Template()
     */
    public function indexAction()
    {
        $repository = $this->get('repository.genre');
        $genres = $repository->findBy(array(), array('id' => 'ASC'), 30);
        return array(
            'genres' => $genres,
        );
    }

    /**
     * @Route("/add", name="_genre_add")
     * @Template()
     */
    public function addAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $manager = $this->get('manager.genre');
        $genre = $manager->create();
        $form = $this->createForm(new GenreType(), $genre);
        $form->handleRequest($request);
        if ($request->getMethod() == 'POST') {
            if ($form->isValid()) {
                $manager->register($genre);

                return $this->redirect($this->generateUrl('_genre_view', array('id' => $genre->getId(),
                )));
            }
        }
        return array(
            'genre' => $genre,
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/edit/{id}", name="_genre_edit")
     * @Template()
     */
    public function editAction($id, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $repository = $this->get('repository.genre');
        $manager = $this->get('manager.genre');

        $genre = $repository->findOneById($id);

        if (!$genre instanceof Genre) {
            $this->redirect('/');
        }

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new GenreType(), $genre);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($genre);
            $em->flush();
        }
        return array(
            'genre' => $genre,
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/delete/{id}", name="_genre_delete")
     * @Template()
     */
    public function deleteAction($id, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $repository = $this->get('repository.genre');
        $item = $repository->findOneById($id);
        $em = $this->getDoctrine()->getManager();
        foreach ($item->getSongs() as $song) {
            $em->remove($song);
        }
        $em->flush();
        foreach ($item->getAlbums() as $song) {
            $em->remove($song);
        }
        $em->flush();
        $manager = $this->get('manager.genre');
        $manager->remove($item);
        return $this->redirect('/');
    }

    /**
     * @Route("/{id}", name="_genre_view", requirements={"id" = "\d+"})
     * @Template()
     */
    public function viewAction($id)
    {
        $repository = $this->get('repository.genre');
        $genre = $repository->findOneById($id);
        if ($genre instanceof Genre) {
            return array(
                'genre' => $genre,
            );
        } else {
            return $this->redirect('/');
        }
    }

}

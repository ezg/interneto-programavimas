<?php

namespace MusicBundle\Manager;

use MusicBundle\Entity\Album;
use MusicBundle\EntityRepository\AlbumRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

/**
 * Class AlbumManager
 * @package MusicBundle\Manager
 */
class AlbumManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var EncoderFactory
     */
    private $encodeFactory;

    /**
     * @var AlbumRepository
     */
    private $albumRepository;

    /**
     * @param EntityManager $entityManager
     * @param EncoderFactory $encodeFactory
     * @param AlbumRepository $albumRepository
     */
    public function __construct(
        EntityManager $entityManager,
        EncoderFactory $encodeFactory,
        AlbumRepository $albumRepository
    ) {
        $this->entityManager = $entityManager;
        $this->encodeFactory = $encodeFactory;
        $this->albumRepository = $albumRepository;
    }

    /**
     * @return Album
     */
    public function create()
    {
        $album = new Album();

        return $album;
    }

    /**
     * @param Album $album
     * @return Album
     */
    public function register(Album $album)
    {
        $this->entityManager->persist($album);
        $this->entityManager->flush();

        return $album;
    }

    /**
     * @param Album $album
     * @return $this
     */
    public function update(Album $album)
    {
        $this->entityManager->persist($album);
        $this->entityManager->flush();

        return $this;
    }

    /**
     * @param Album $album or Array of albums Ids
     * @return $this
     */
    public function remove($album)
    {
        if ($album instanceof Album) {
            $this->entityManager->remove($album);
        } elseif (is_array($album)) {
            foreach ($album as $albumId) {
                if (is_numeric($albumId)) {
                    $albumEntity = $this->albumRepository->findOneById($albumId);
                    $this->entityManager->remove($albumEntity);
                }
            }
        } elseif (is_numeric($album)) {
            $albumEntity = $this->albumRepository->findOneById($album);
            $this->entityManager->remove($albumEntity);
        }

        $this->entityManager->flush();
        return $this;
    }
}
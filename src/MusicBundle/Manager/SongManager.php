<?php

namespace MusicBundle\Manager;

use MusicBundle\Entity\Song;
use MusicBundle\EntityRepository\SongRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

/**
 * Class SongManager
 * @package MusicBundle\Manager
 */
class SongManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var EncoderFactory
     */
    private $encodeFactory;

    /**
     * @var SongRepository
     */
    private $songRepository;

    /**
     * @param EntityManager $entityManager
     * @param EncoderFactory $encodeFactory
     * @param SongRepository $songRepository
     */
    public function __construct(
        EntityManager $entityManager,
        EncoderFactory $encodeFactory,
        SongRepository $songRepository
    ) {
        $this->entityManager = $entityManager;
        $this->encodeFactory = $encodeFactory;
        $this->songRepository = $songRepository;
    }

    /**
     * @return Song
     */
    public function create()
    {
        $song = new Song();

        return $song;
    }

    /**
     * @param Song $song
     * @return Song
     */
    public function register(Song $song)
    {
        $this->entityManager->persist($song);
        $this->entityManager->flush();

        return $song;
    }

    /**
     * @param Song $song
     * @return $this
     */
    public function update(Song $song)
    {
        $this->entityManager->persist($song);
        $this->entityManager->flush();

        return $this;
    }

    /**
     * @param Song $song or Array of songs Ids
     * @return $this
     */
    public function remove($song)
    {
        if ($song instanceof Song) {
            $this->entityManager->remove($song);
        } elseif (is_array($song)) {
            foreach ($song as $songId) {
                if (is_numeric($songId)) {
                    $songEntity = $this->songRepository->findOneById($songId);
                    $this->entityManager->remove($songEntity);
                }
            }
        } elseif (is_numeric($song)) {
            $songEntity = $this->songRepository->findOneById($song);
            $this->entityManager->remove($songEntity);
        }

        $this->entityManager->flush();
        return $this;
    }
}
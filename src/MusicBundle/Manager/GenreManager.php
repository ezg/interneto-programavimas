<?php

namespace MusicBundle\Manager;

use MusicBundle\Entity\Genre;
use MusicBundle\EntityRepository\GenreRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

/**
 * Class GenreManager
 * @package MusicBundle\Manager
 */
class GenreManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var EncoderFactory
     */
    private $encodeFactory;

    /**
     * @var GenreRepository
     */
    private $genreRepository;

    /**
     * @param EntityManager $entityManager
     * @param EncoderFactory $encodeFactory
     * @param GenreRepository $genreRepository
     */
    public function __construct(
        EntityManager $entityManager,
        EncoderFactory $encodeFactory,
        GenreRepository $genreRepository
    ) {
        $this->entityManager = $entityManager;
        $this->encodeFactory = $encodeFactory;
        $this->genreRepository = $genreRepository;
    }

    /**
     * @return Genre
     */
    public function create()
    {
        $genre = new Genre();

        return $genre;
    }

    /**
     * @param Genre $genre
     * @return Genre
     */
    public function register(Genre $genre)
    {
        $this->entityManager->persist($genre);
        $this->entityManager->flush();

        return $genre;
    }

    /**
     * @param Genre $genre
     * @return $this
     */
    public function update(Genre $genre)
    {
        $this->entityManager->persist($genre);
        $this->entityManager->flush();

        return $this;
    }

    /**
     * @param Genre $genre or Array of genres Ids
     * @return $this
     */
    public function remove($genre)
    {
        if ($genre instanceof Genre) {
            $this->entityManager->remove($genre);
        } elseif (is_array($genre)) {
            foreach ($genre as $genreId) {
                if (is_numeric($genreId)) {
                    $genreEntity = $this->genreRepository->findOneById($genreId);
                    $this->entityManager->remove($genreEntity);
                }
            }
        } elseif (is_numeric($genre)) {
            $genreEntity = $this->genreRepository->findOneById($genre);
            $this->entityManager->remove($genreEntity);
        }

        $this->entityManager->flush();
        return $this;
    }
}
<?php

namespace MusicBundle\Manager;

use MusicBundle\Entity\Artist;
use MusicBundle\EntityRepository\ArtistRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

/**
 * Class ArtistManager
 * @package MusicBundle\Manager
 */
class ArtistManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var EncoderFactory
     */
    private $encodeFactory;

    /**
     * @var ArtistRepository
     */
    private $artistRepository;

    /**
     * @param EntityManager $entityManager
     * @param EncoderFactory $encodeFactory
     * @param ArtistRepository $artistRepository
     */
    public function __construct(
        EntityManager $entityManager,
        EncoderFactory $encodeFactory,
        ArtistRepository $artistRepository
    ) {
        $this->entityManager = $entityManager;
        $this->encodeFactory = $encodeFactory;
        $this->artistRepository = $artistRepository;
    }

    /**
     * @return Artist
     */
    public function create()
    {
        $artist = new Artist();

        return $artist;
    }

    /**
     * @param Artist $artist
     * @return Artist
     */
    public function register(Artist $artist)
    {
        $this->entityManager->persist($artist);
        $this->entityManager->flush();

        return $artist;
    }

    /**
     * @param Artist $artist
     * @return $this
     */
    public function update(Artist $artist)
    {
        $this->entityManager->persist($artist);
        $this->entityManager->flush();

        return $this;
    }

    /**
     * @param Artist $artist or Array of artists Ids
     * @return $this
     */
    public function remove($artist)
    {
        if ($artist instanceof Artist) {
            $this->entityManager->remove($artist);
        } elseif (is_array($artist)) {
            foreach ($artist as $artistId) {
                if (is_numeric($artistId)) {
                    $artistEntity = $this->artistRepository->findOneById($artistId);
                    $this->entityManager->remove($artistEntity);
                }
            }
        } elseif (is_numeric($artist)) {
            $artistEntity = $this->artistRepository->findOneById($artist);
            $this->entityManager->remove($artistEntity);
        }

        $this->entityManager->flush();
        return $this;
    }
}
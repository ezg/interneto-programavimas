<?php

namespace MusicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Genre
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="MusicBundle\EntityRepository\GenreRepository")
 */
class Genre
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptionUrl", type="string", length=255)
     */
    private $descriptionUrl;

    /**
     * @ORM\OneToMany(targetEntity="Album", mappedBy="genre")
     */
    private $albums;

    /**
     * @ORM\OneToMany(targetEntity="Song", mappedBy="genre")
     */
    private $songs;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Genre
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Genre
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set descriptionUrl
     *
     * @param string $descriptionUrl
     * @return Genre
     */
    public function setDescriptionUrl($descriptionUrl)
    {
        $this->descriptionUrl = $descriptionUrl;

        return $this;
    }

    /**
     * Get descriptionUrl
     *
     * @return string 
     */
    public function getDescriptionUrl()
    {
        return $this->descriptionUrl;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->albums = new \Doctrine\Common\Collections\ArrayCollection();
        $this->songs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add albums
     *
     * @param \MusicBundle\Entity\Album $albums
     * @return Genre
     */
    public function addAlbum(\MusicBundle\Entity\Album $albums)
    {
        $this->albums[] = $albums;

        return $this;
    }

    /**
     * Remove albums
     *
     * @param \MusicBundle\Entity\Album $albums
     */
    public function removeAlbum(\MusicBundle\Entity\Album $albums)
    {
        $this->albums->removeElement($albums);
    }

    /**
     * Get albums
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAlbums()
    {
        return $this->albums;
    }

    /**
     * Add songs
     *
     * @param \MusicBundle\Entity\Song $songs
     * @return Genre
     */
    public function addSong(\MusicBundle\Entity\Song $songs)
    {
        $this->songs[] = $songs;

        return $this;
    }

    /**
     * Remove songs
     *
     * @param \MusicBundle\Entity\Song $songs
     */
    public function removeSong(\MusicBundle\Entity\Song $songs)
    {
        $this->songs->removeElement($songs);
    }

    /**
     * Get songs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSongs()
    {
        return $this->songs;
    }

    public function __toString()
    {
        return $this->getName();
    }
}

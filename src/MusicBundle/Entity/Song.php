<?php

namespace MusicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Song
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="MusicBundle\EntityRepository\SongRepository")
 */
class Song
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="releaseYear", type="string", length=4)
     */
    private $releaseYear;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="youtubeUrl", type="string", length=255)
     */
    private $youtubeUrl;

    /**
     * @ORM\ManyToOne(targetEntity="Album", inversedBy="songs")
     */
    private $album;

    /**
     * @ORM\ManyToMany(targetEntity="PlaylistBundle\Entity\Playlist", mappedBy="songs")
     */
    private $playlists;

    /**
     * @ORM\ManyToOne(targetEntity="MusicBundle\Entity\Genre", inversedBy="songs")
     */
    private $genre;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Song
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set releaseYear
     *
     * @param string $releaseYear
     * @return Song
     */
    public function setReleaseYear($releaseYear)
    {
        $this->releaseYear = $releaseYear;

        return $this;
    }

    /**
     * Get releaseYear
     *
     * @return string 
     */
    public function getReleaseYear()
    {
        return $this->releaseYear;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Song
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set youtubeUrl
     *
     * @param string $youtubeUrl
     * @return Song
     */
    public function setYoutubeUrl($youtubeUrl)
    {
        $this->youtubeUrl = $youtubeUrl;

        return $this;
    }

    /**
     * Get youtubeUrl
     *
     * @return string 
     */
    public function getYoutubeUrl()
    {
        return $this->youtubeUrl;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->playlists = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set album
     *
     * @param \MusicBundle\Entity\Album $album
     * @return Song
     */
    public function setAlbum(\MusicBundle\Entity\Album $album = null)
    {
        $this->album = $album;

        return $this;
    }

    /**
     * Get album
     *
     * @return \MusicBundle\Entity\Album 
     */
    public function getAlbum()
    {
        return $this->album;
    }

    /**
     * Add playlists
     *
     * @param \PlaylistBundle\Entity\Playlist $playlists
     * @return Song
     */
    public function addPlaylist(\PlaylistBundle\Entity\Playlist $playlists)
    {
        $this->playlists[] = $playlists;

        return $this;
    }

    /**
     * Remove playlists
     *
     * @param \PlaylistBundle\Entity\Playlist $playlists
     */
    public function removePlaylist(\PlaylistBundle\Entity\Playlist $playlists)
    {
        $this->playlists->removeElement($playlists);
    }

    /**
     * Get playlists
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlaylists()
    {
        return $this->playlists;
    }

    /**
     * Set genre
     *
     * @param \MusicBundle\Entity\Genre $genre
     * @return Song
     */
    public function setGenre(\MusicBundle\Entity\Genre $genre = null)
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * Get genre
     *
     * @return \MusicBundle\Entity\Genre 
     */
    public function getGenre()
    {
        return $this->genre;
    }

    public function __toString()
    {
        return $this->getName();
    }
}

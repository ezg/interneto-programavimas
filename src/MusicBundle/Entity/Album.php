<?php

namespace MusicBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Album
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="MusicBundle\EntityRepository\AlbumRepository")
 */
class Album
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="releaseYear", type="string", length=4)
     */
    private $releaseYear;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptionUrl", type="string", length=255)
     */
    private $descriptionUrl;

    /**
     * @ORM\ManyToOne(targetEntity="Artist", inversedBy="albums")
     */
    private $artist;

    /**
     * @ORM\ManyToOne(targetEntity="Genre", inversedBy="albums")
     */
    private $genre;

    /**
     * @ORM\OneToMany(targetEntity="Song", mappedBy="album")
     */
    private $songs;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Album
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set releaseYear
     *
     * @param string $releaseYear
     * @return Album
     */
    public function setReleaseYear($releaseYear)
    {
        $this->releaseYear = $releaseYear;

        return $this;
    }

    /**
     * Get releaseYear
     *
     * @return string 
     */
    public function getReleaseYear()
    {
        return $this->releaseYear;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Album
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set descriptionUrl
     *
     * @param string $descriptionUrl
     * @return Album
     */
    public function setDescriptionUrl($descriptionUrl)
    {
        $this->descriptionUrl = $descriptionUrl;

        return $this;
    }

    /**
     * Get descriptionUrl
     *
     * @return string 
     */
    public function getDescriptionUrl()
    {
        return $this->descriptionUrl;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->songs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set artist
     *
     * @param \MusicBundle\Entity\Artist $artist
     * @return Album
     */
    public function setArtist(\MusicBundle\Entity\Artist $artist = null)
    {
        $this->artist = $artist;

        return $this;
    }

    /**
     * Get artist
     *
     * @return \MusicBundle\Entity\Artist 
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * Set genre
     *
     * @param \MusicBundle\Entity\Genre $genre
     * @return Album
     */
    public function setGenre(\MusicBundle\Entity\Genre $genre = null)
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * Get genre
     *
     * @return \MusicBundle\Entity\Genre 
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * Add songs
     *
     * @param \MusicBundle\Entity\Song $songs
     * @return Album
     */
    public function addSong(\MusicBundle\Entity\Song $songs)
    {
        $this->songs[] = $songs;

        return $this;
    }

    /**
     * Remove songs
     *
     * @param \MusicBundle\Entity\Song $songs
     */
    public function removeSong(\MusicBundle\Entity\Song $songs)
    {
        $this->songs->removeElement($songs);
    }

    /**
     * Get songs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSongs()
    {
        return $this->songs;
    }

    public function __toString()
    {
        return $this->getName();
    }
}

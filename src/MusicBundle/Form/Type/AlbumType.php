<?php

namespace MusicBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class AlbumType
 * @package MusicBundle\Form\Type
 */
class AlbumType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('releaseYear')
            ->add('description')
            ->add('descriptionUrl')
            ->add('artist')
            ->add('genre')
            ->add('save', 'submit', [
                'label' => 'Išsaugoti',
            ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MusicBundle\Entity\Album',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'form_album_type';
    }
}